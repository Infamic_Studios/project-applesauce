# Project- Applesauce#

Project Applesauce is a WIP modpack, built (almost) from the ground up to allow for dynamic 
experiences and player driven gameplay.

### What is this repository for? ###

This Master repository is for developers and those interested in contributing to Project Applesauce. 
If your interest is solely to download and run Project Applesauce you can find release builds and 
installation instructions on the releases branch.

### How do I get set up? ###

If you are interested in contributing to project applesauce here is a quick rundown on how you can get setup!

#### Release Configuration: ####
1. Clone or download the desired zip file from the releases.
2. Download and install [MultiMc](https://multimc.org). 
3. Launch MultiMc and click import instance.
4. Select the downloaded zip file.
5. Log in to MultiMc with your Minecraft Credentials
6. Have Fun! :)

#### Development Workspace Configuration: ####
1. Clone or download this master branch.
2. Download and install [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) NOTE: Download Community Edition
3. Install IDEA and import the build.gradle file contained in this branch as an existing project.
4. Ensure that the workspace JDK is 1.8 or 10 
5. Import the project and run 'SetupDecompWorkspace' in the tasks branch under the gradle settings tree.
6. Set the 'runclient' and 'runserver' configurations to use the projectapplesauce.main class directory.
7. All Set!

### Contribution guidelines ###

If you would like to contribute to this project please remember all code contributions must undergo code review and pass unitcheck tests.

some of the principles contributed code should stick to are:
1. KISS (KEEP IT SIMPLE STUPID) IE: Make sure code is readable and optimized for performance.
2. DOCUMENTATION IS KEY IE: Follow Java documentation guidelines.
3. Keep Dependencies at a minimum. By nature of being a modpack, Project applesauce is dependant on a number of dependancies and external code liberaries. one of the core goals is to reduce bloat and maintain performance and as such if possible minimize the usage of external dependencies and codebases. 
4. Have Fun, This is ultimatly a modpack for minecraft, don't take it so seriously.

### Who do I talk to? ###
If you encounter a bug or would like to take part in the the development community of this modpack you can report issues under the issues tab. Thanks!