package com.lycanitesmobs.core.item.temp;

import com.lycanitesmobs.LycanitesMobs;
import com.lycanitesmobs.ObjectManager;
import com.lycanitesmobs.core.entity.BaseProjectileEntity;
import com.lycanitesmobs.core.info.projectile.ProjectileInfo;
import com.lycanitesmobs.core.info.projectile.ProjectileManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemScepterQuill extends ItemScepter {

	// ==================================================
	//                   Constructor
	// ==================================================
    public ItemScepterQuill() {
        super();
    	this.modInfo = LycanitesMobs.modInfo;
    	this.itemName = "quillscepter";
        this.setup();
    }


    // ==================================================
    //                       Use
    // ==================================================
    @Override
    public int getDurability() {
        return 250;
    }

    @Override
    public int getRapidTime(ItemStack itemStack) {
        return 5;
    }


    // ==================================================
    //                      Attack
    // ==================================================
    @Override
    public boolean rapidAttack(ItemStack itemStack, World world, EntityLivingBase entity) {
        if(!world.isRemote) {
            ProjectileInfo projectileInfo = ProjectileManager.getInstance().getProjectile("quill");
            if(projectileInfo == null) {
                return true;
            }
            BaseProjectileEntity projectile = projectileInfo.createProjectile(world, entity);
            world.spawnEntity(projectile);
            this.playSound(itemStack, world, entity, 1, projectile);
        }
        return true;
    }


	// ==================================================
	//                     Repairs
	// ==================================================
    @Override
    public boolean getIsRepairable(ItemStack itemStack, ItemStack repairStack) {
        if(repairStack.getItem() == ObjectManager.getItem("quill")) return true;
        return super.getIsRepairable(itemStack, repairStack);
    }
}
