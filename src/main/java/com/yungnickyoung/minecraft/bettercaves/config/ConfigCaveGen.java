package com.yungnickyoung.minecraft.bettercaves.config;

import com.yungnickyoung.minecraft.bettercaves.config.cave.*;
import com.yungnickyoung.minecraft.bettercaves.config.cavern.ConfigCaverns;
import net.minecraftforge.common.config.Config;

public class ConfigCaveGen {
    @Config.Name("Caves")
    @Config.Comment("Settings used in the generation of the caves in Better Caves.")
    public ConfigCaves caves = new ConfigCaves();

    @Config.Name("Caverns")
    @Config.Comment("Settings used in the generation of the caverns in Better Caves. Caverns are spacious caves that " +
            "spawn at low altitudes.")
    public ConfigCaverns caverns = new ConfigCaverns();

    @Config.Name("Water Regions")
    @Config.Comment("Parameters used in the generation of water-based caves and caverns. These are similar to " +
            "the regular Better Caves and Caverns, but with water instead of lava.")
    public ConfigWaterRegions waterRegions = new ConfigWaterRegions();

    @Config.Name("Whitelisted Dimension IDs")
    @Config.Comment("List of ID's of dimensions that will have Better Caves. Ignored if Global Whitelisting is enabled.")
    @Config.RequiresWorldRestart
    public int[] whitelistedDimensionIDs = {0};

    @Config.Name("Enable Global Whitelist")
    @Config.Comment("Automatically enables Better Caves in every possible dimension, except for the Nether and End. " +
            "If this is enabled, the Whitelisted Dimension IDs option is ignored.")
    @Config.RequiresWorldRestart
    public boolean enableGlobalWhitelist = false;
}
