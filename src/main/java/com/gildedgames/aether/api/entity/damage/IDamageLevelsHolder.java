package com.gildedgames.aether.api.entity.damage;

public interface IDamageLevelsHolder
{
	int getSlashDamageLevel();

	int getPierceDamageLevel();

	int getImpactDamageLevel();
}
