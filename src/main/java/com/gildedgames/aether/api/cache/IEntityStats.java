package com.gildedgames.aether.api.cache;

public interface IEntityStats
{
	float getMaxHealth();

	double getSlashDefenseLevel();

	double getPierceDefenseLevel();

	double getImpactDefenseLevel();
}
